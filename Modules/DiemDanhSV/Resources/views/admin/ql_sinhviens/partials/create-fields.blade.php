<div class="box-body">
		<div class="row">
			<div class="col-sm-4">
				<label>MSSV</label>
				<input type="text" name="mssv" class="form-control" placeholder="Mã số sinh viên" ><span id="mssv" style="color:red"></span><br>
				<label>Họ Đệm</label>
				<input type="text" name="hodem" class="form-control" placeholder="Họ Đệm" ><span id="hodem" style="color:red"></span><br>
				<label>Tên</label>
				<input type="text" name="ten" class="form-control" placeholder="Tên" ><span id="ten" style="color:red"></span><br>
				<label>Lớp</label>
				<input type="text" name="lop" class="form-control" placeholder="Lớp" ><span id="lop" style="color:red"></span><br>
			</div>
			<div class="col-sm-4">
				<label>Hình Ảnh</label><br/>
				<label class="file">
					<input type="file" name="hinh" aria-label="File browser example" >
					<span class="file-custom"></span>
				</label><br>
				<span id="fimage" style="color:red;text-align: right;"></span>
			</div>
		</div>
</div>

