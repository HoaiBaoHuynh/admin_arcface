@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('diemdanhsv::ql_sinhviens.title.ql_sinhviens') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('diemdanhsv::ql_sinhviens.title.ql_sinhviens') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.diemdanhsv.ql_sinhvien.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('diemdanhsv::ql_sinhviens.button.create ql_sinhvien') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="text-align: center;vertical-align: middle;">Id</th>
                                    <th style="text-align: center;vertical-align: middle;">Image</th>
                                    <th style="text-align: center;vertical-align: middle;">MSSV</th>
                                    <th style="text-align: center;vertical-align: middle;">Họ Đệm</th>
                                    <th style="text-align: center;vertical-align: middle;">Tên</th>
                                    <th style="text-align: center;vertical-align: middle;">Lớp</th>
                                    <th data-sortable="false" style="text-align: center; vertical-align: middle;">{{ trans('core::core.table.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($ql_sinhviens)): ?>
                            <?php foreach ($ql_sinhviens as $ql_sinhvien): ?>
                            <tr>
                                <td style="text-align: center; vertical-align: middle;">
                                    <a href="{{ route('admin.diemdanhsv.ql_sinhvien.edit', [$ql_sinhvien->id]) }}">
                                        {{ $ql_sinhvien->id }}
                                    </a>
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    <div class="single-item-header">
                                        <img src="../../../assets/images/{{$ql_sinhvien->image}}" height="90px" width="90px" />
                                    </div>
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    <a href="{{ route('admin.diemdanhsv.ql_sinhvien.edit', [$ql_sinhvien->id]) }}">
                                        {{ $ql_sinhvien->MSSV }}
                                    </a>
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    <a href="{{ route('admin.diemdanhsv.ql_sinhvien.edit', [$ql_sinhvien->id]) }}">
                                        {{ $ql_sinhvien->HoDem }}
                                    </a>
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    <a href="{{ route('admin.diemdanhsv.ql_sinhvien.edit', [$ql_sinhvien->id]) }}">
                                        {{ $ql_sinhvien->Ten }}
                                    </a>
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    <a href="{{ route('admin.diemdanhsv.ql_sinhvien.edit', [$ql_sinhvien->id]) }}">
                                        {{ $ql_sinhvien->Lop }}
                                    </a>
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    <div class="btn-group">
                                        <a href="{{ route('admin.diemdanhsv.ql_sinhvien.edit', [$ql_sinhvien->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.diemdanhsv.ql_sinhvien.destroy', [$ql_sinhvien->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('diemdanhsv::ql_sinhviens.title.create ql_sinhvien') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.diemdanhsv.ql_sinhvien.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "asc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
