@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('diemdanhsv::real_time_recognitions.title.real_time_recognitions') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('diemdanhsv::real_time_recognitions.title.real_time_recognitions') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="text-align: center;vertical-align: middle;">STT</th>
                                    <th style="text-align: center;vertical-align: middle;">Image</th>
                                    <th style="text-align: center;vertical-align: middle;">Image_Compare</th>
                                    <th style="text-align: center;vertical-align: middle;">MSSV</th>
                                    <th style="text-align: center;vertical-align: middle;">Họ Đệm</th>
                                    <th style="text-align: center;vertical-align: middle;">Tên</th>
                                    <th style="text-align: center;vertical-align: middle;">Lớp</th>
                                    <th style="text-align: center;vertical-align: middle;">Check in</th>
                                </tr>
                            </thead>
                            <tbody id="student-info">
                            <?php if (isset($values)): ?>
                            <?php $i=1; ?>
                            <?php foreach ($values as $real_time_recognition):?>
                            <tr>
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $i }}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    <div class="single-item-header">
                                        <img src="../../../assets/images/{{$real_time_recognition->image}}" height="90px" width="90px" />
                                    </div>
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    <div class="single-item-header">
                                        <img src="../../../assets/image_compare/{{$real_time_recognition->img_compare}}" height="90px" width="90px"/>
                                    </div>
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $real_time_recognition->MSSV }}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $real_time_recognition->HoDem }}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $real_time_recognition->Ten }}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $real_time_recognition->Lop }}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    {{date('d-m-Y H:i:s',strtotime(str_replace('/','-',$real_time_recognition->time)))}}
                                </td>
                            </tr>
                            <?php $i++; ?>
                            <?php  endforeach;?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="text-align: center;vertical-align: middle;">STT</th>
                                    <th style="text-align: center;vertical-align: middle;">Image</th>
                                    <th style="text-align: center;vertical-align: middle;">Tên</th>
                                    <th style="text-align: center;vertical-align: middle;">Check in</th>
                                </tr>
                            </thead>
                            <tbody id="unknown-info">
                            <?php if (isset($values2)): ?>
                            <?php $i=1; ?>
                            <?php foreach ($values2 as $real_time_recognition):?>
                            <tr>
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $i }}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    <div class="single-item-header">
                                        <img src="../../../assets/image_compare/{{$real_time_recognition->image}}" height="90px" width="90px"/>
                                    </div>
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $real_time_recognition->MSSV }}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    {{date('d-m-Y H:i:s',strtotime(str_replace('/','-',$real_time_recognition->time)))}}
                                </td>
                            </tr>
                            <?php $i++; ?>
                            <?php  endforeach;?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@stop
@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('diemdanhsv::real_time_recognitions.title.create real_time_recognition') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.diemdanhsv.real_time_recognition.create') ?>" }
                ]
            });
        });
    </script>
    <script type="text/javascript">
        init_reload();
        function init_reload(){
            setInterval( function() {
                $.get("{{ route('admin.diemdanhsv.real_time_recognition.ajax') }}",function(data){
                    $('#student-info').empty().html(data);
                })

                $.get("{{ route('admin.diemdanhsv.real_time_recognition.unkown') }}",function(data){
                    $('#unknown-info').empty().html(data);
                })
          },3000);
    }
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
