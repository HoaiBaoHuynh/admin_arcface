<?php if (isset($values2)): ?>
<?php $i=1; ?>
<?php foreach ($values2 as $real_time_recognition):?>
    <tr>
        <td style="text-align: center; vertical-align: middle;">
            {{ $i }}
        </td>
        <td style="text-align: center; vertical-align: middle;">
            <div class="single-item-header">
                <img src="../../../assets/image_compare/{{$real_time_recognition->image}}" height="90px" width="90px"/>
            </div>
        </td>
        <td style="text-align: center; vertical-align: middle;">
            {{ $real_time_recognition->MSSV }}
        </td>
        <td style="text-align: center; vertical-align: middle;">
            {{date('d-m-Y H:i:s',strtotime(str_replace('/','-',$real_time_recognition->time)))}}
        </td>
    </tr>
<?php $i++; ?>
<?php  endforeach;?>
<?php endif; ?>