<?php if (isset($values)): ?>
<?php $i=1; ?>
<?php foreach ($values as $real_time_recognition):?>
    <tr>
        <td style="text-align: center; vertical-align: middle;">
            {{ $i }}
        </td>
        <td style="text-align: center; vertical-align: middle;">
            <div class="single-item-header">
                <img src="../../../assets/images/{{$real_time_recognition->image}}" height="90px" width="90px" />
            </div>
        </td>
        <td style="text-align: center; vertical-align: middle;">
            <div class="single-item-header">
                <img src="../../../assets/image_compare/{{$real_time_recognition->img_compare}}" height="90px" width="90px" />
            </div>
        </td>
        <td style="text-align: center; vertical-align: middle;">
            {{ $real_time_recognition->MSSV }}
        </td>
        <td style="text-align: center; vertical-align: middle;">
            {{ $real_time_recognition->HoDem }}
        </td>
        <td style="text-align: center; vertical-align: middle;">
            {{ $real_time_recognition->Ten }}
        </td>
        <td style="text-align: center; vertical-align: middle;">
            {{ $real_time_recognition->Lop }}
        </td>
        <td style="text-align: center; vertical-align: middle;">
            {{date('d-m-Y H:i:s',strtotime(str_replace('/','-',$real_time_recognition->time)))}}
        </td>
    </tr>
<?php $i++; ?>
<?php  endforeach;?>
<?php endif; ?>