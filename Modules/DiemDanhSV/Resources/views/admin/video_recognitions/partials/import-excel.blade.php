<style>
	select {
		font-family: Arial;
		margin-bottom: 1em;
		padding: .25em;
		border: 0;
		border-bottom: 2px solid currentcolor; 
		font-weight: bold;
		letter-spacing: .15em;
		border-radius: 0;
		&:focus, &:active {
			outline: 0;
			border-bottom-color: red;
		}
	}
</style>
<div class="box-body">
	@if(count($errors) > 0)
	<div class="alert alert-danger">
		Upload Validation Error<br>
		<button type="button" class="close" data-dismiss="alert">×</button><br>
		<ul>
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<table class="table-responsive">
		<tbody>
			<tr>
				<td style="padding: 5px;">
					<div class="form-group">
						<table class="table">
							<tr>
								<td colspan="2" align="center">
									<label style="font-size: 24px;"><p>Điểm Danh Sinh Viên Bằng Camera</p></label>
								</td>
							</tr>
							<form method="post" name="importform" enctype="multipart/form-data" action="{{ route('admin.diemdanhsv.video_recognition.camera_start')}}">
								{{ csrf_field() }}
							<tr>
								<td align="right"><label>Chọn tuần cần điểm danh</label></td>
								<td>
									<select name="tuan" class="select" required="true">
										<option value="" selected="selected" disabled="true">Hãy  Chọn Tuần</option>
										@foreach($dsTuan as $key)
										<option value="{{$key->name}}">Tuần {{$key->id}}</option>
										@endforeach
									</select>
								</td>
							</tr>
							<tr>
								<td align="right">
									<input type="submit" name="upload_camera1" id="myButton" class="btn btn-success" value="Start">
								</td>
							</form>
								<td align="left">
									<input type="submit" id="enableButton" name="upload_camera2" class="btn btn-danger" value="End" disabled="disabled">
								</td>
							</tr>
						</table>
					</div>
				</td>
				<td style="padding: 5px;">
					<form method="post" enctype="multipart/form-data" action="{{ route('admin.diemdanhsv.video_recognition.diemdanh_hinhanh') }}">
						{{ csrf_field() }}
						<div class="form-group">
							<table class="table">
								<tr>
									<td colspan="2" align="center">
										<label style="font-size: 24px;"><p>Điểm Danh Sinh Viên Bằng Ảnh</p></label>
									</td>
								</tr>
								<tr>
									<td align="right"><label>Chọn hình ảnh </label></td>
									<td>
										<input type="file" name="image_file[]" multiple />
									</td>
								</tr>
								<tr>
									<td colspan="2" align="center"><span class="text-muted">.jpg, .png, .jpeg, ...</span></td>
								</tr>
								<tr>
									<td align="right"><label>Chọn tuần cần điểm danh</label></td>
									<td>
										<select name="tuan" class="select" required="true">
											<option value="" selected="selected" disabled="true">Hãy  Chọn Tuần</option>
											@foreach($dsTuan as $key)
											<option value="{{$key->name}}">Tuần {{$key->id}}</option>
											@endforeach
										</select>
									</td>
								</tr>
								<tr>
									<td colspan="2" align="center">
										<input type="submit" name="img_check" class="btn btn-primary" value="Điểm Danh">
									</td>
								</tr>
							</table>
						</div>
					</form>
				</td>
				<td style="padding: 5px;">
					<form method="post" enctype="multipart/form-data" action="{{ route('admin.diemdanhsv.video_recognition.diemdanh_video') }}">
						{{ csrf_field() }}
						<div class="form-group">
							<table class="table">
								<tr>
									<td colspan="2" align="center">
										<label style="font-size: 24px;"><p>Điểm Danh Sinh Viên Bằng Video</p></label>
									</td>
								</tr>
								<tr>
									<td align="right"><label>Chọn video </label></td>
									<td>
										<input type="file" name="video_file" />
									</td>
								</tr>
								<tr>
									<td colspan="2" align="center"><span class="text-muted">.mp4, .avi,webm ...</span></td>
								</tr>
								<tr hidden>
									<td align="left"><label>Chọn mã lớp cần điểm danh</label></td>
									<td>
										<select name="malop_old" class="select">
											@foreach($dsMaLop as $value)
												<option value="{{$value->ma_lop}}">{{$value->ma_lop}}</option>
											@endforeach
										</select>
									</td>
								</tr>
								<tr>
									<td align="right"><label>Chọn tuần cần điểm danh</label></td>
									<td>
										<select name="tuan" class="select" required="true">
											<option value="" selected="selected" disabled="true">Hãy  Chọn Tuần</option>
											@foreach($dsTuan as $key)
											<option value="{{$key->name}}">Tuần {{$key->id}}</option>
											@endforeach
										</select>
									</td>
								</tr>
								<tr>
									<td colspan="2" align="center">
										<input type="submit" name="upload_video" class="btn btn-primary" value="Điểm Danh">
									</td>
								</tr>
							</table>
						</div>
					</form>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<script>
	$(document).ready( function(){
		var clicked = false;

		$('#myButton').click(function() {
			if(clicked === false) {
				/*$(this).attr('disabled','disabled');*/
				$('#enableButton').removeAttr('disabled');
				clicked = true;
			}
		});

		$('#enableButton').click(function() {
			// $('#myButton').removeAttr('disabled');
			$('#enableButton').attr('disabled','disabled');
			$.get("{{ route('admin.diemdanhsv.video_recognition.camera_end') }}",function(data){
				alert("Kết thúc điểm danh.");
			})
			clicked = false; 
		});
	});
</script>