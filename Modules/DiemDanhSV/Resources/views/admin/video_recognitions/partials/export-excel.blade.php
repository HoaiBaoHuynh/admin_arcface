<style>
	select {
		font-family: Arial;
		margin-bottom: 1em;
		padding: .25em;
		border: 0;
		border-bottom: 2px solid currentcolor; 
		font-weight: bold;
		letter-spacing: .15em;
		border-radius: 0;
		&:focus, &:active {
			outline: 0;
			border-bottom-color: red;
		}
	}
</style>
<div class="box box-primary">
	<div class="box-body">
		<tbody>
			<table class="table-responsive" style="margin-left: 430px;">
				<tr>
					<td style="padding: 5px;">
						<form method="post" name="importform" enctype="multipart/form-data" action="{{ route('admin.diemdanhsv.video_recognition.import_excel') }}">
							{{ csrf_field() }}
							<div class="form-group">
								<table class="table">
									<tr>
										<td colspan="2" align="center">
											<label style="font-size: 24px;"><p>Nhập File Excel</p></label>
										</td>
									</tr>
									<tr>
										<td align="right"><label>Chọn file excel cần upload</label></td>
										<td align="left">
											<input type="file" name="select_file[]" multiple/>
										</td>
									</tr>
									<tr>
										<td colspan="2" align="center"><span class="text-muted">.xls, .xslx</span></td>
									</tr>
									<tr>
										<td colspan="2" align="center" >
											<input type="submit" name="upload" class="btn btn-primary" value="Import Excel">
										</td>
									</tr>
								</table>
							</div>
						</form>
					</td>
				</tr>
			</table>
		</tbody>
	</div>
</div>

<div class="box-body">
	<form method="POST" enctype="multipart/form-data" action="{{ route('admin.diemdanhsv.video_recognition.export_excels') }}">
		{{ csrf_field() }}
		<div class="table">
			<tr>
				<td ><label hidden style="margin-left: 800px;">Chọn mã lớp cần xuất: </label></td>
				<td align="right">
					<select name="malop_1" class="select" hidden style="margin-left: 10px;">
						@foreach($dsMaLop as $value)
						<option value="{{$value->ma_lop}}">{{$value->ma_lop}}</option>
						@endforeach
					</select>
				</td>
				<td>
					<div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
						<input type="submit" class="btn btn-primary btn-flat fa fa-pencil" value="{{ trans('diemdanhsv::video_recognitions.button.export excel') }}" style="padding: 4px 10px;height: 27px; text-align: justify;">
					</div>
				</td>
			</tr>
		</div>
	</form>
</div>
