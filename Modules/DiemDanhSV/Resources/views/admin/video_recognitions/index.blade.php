@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('diemdanhsv::video_recognitions.title.video_recognitions') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('diemdanhsv::video_recognitions.title.video_recognitions') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    @include('diemdanhsv::admin.video_recognitions.partials.import-excel')
                </div>
            </div>
            <div class="row">
                @include('diemdanhsv::admin.video_recognitions.partials.export-excel')
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="text-align: center;vertical-align: middle;">Id</th>
                                    <th style="text-align: center;vertical-align: middle;">MSSV</th>
                                    <th style="text-align: center;vertical-align: middle;">Mã Lớp</th>
                                    <th style="text-align: center;vertical-align: middle;">Họ Đệm</th>
                                    <th style="text-align: center;vertical-align: middle;">Tên</th>
                                    <th style="text-align: center;vertical-align: middle;">Lớp</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 1</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 2</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 3</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 4</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 5</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 6</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 7</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 8</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 9</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 10</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 11</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 12</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 13</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 14</th>
                                    <th style="text-align: center;vertical-align: middle;">Tuần 15</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($video_recognitions)): ?>
                            <?php foreach ($video_recognitions as $video_recognition): ?>
                            <tr>
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->id }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->MSSV }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Ma_Lop }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->HoDem }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Ten }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Lop }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_1 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_2 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_3 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_4 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_5 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_6 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_7 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_8 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_9 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_10 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_11 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_12 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_13 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_14 }}
                                </td >
                                <td style="text-align: center; vertical-align: middle;">
                                    {{ $video_recognition->Tuan_15 }}
                                </td >
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('diemdanhsv::video_recognitions.title.create video_recognition') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.diemdanhsv.video_recognition.index') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "asc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
