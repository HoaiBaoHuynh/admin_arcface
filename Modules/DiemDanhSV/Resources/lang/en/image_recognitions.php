<?php

return [
    'list resource' => 'List image_recognitions',
    'create resource' => 'Create image_recognitions',
    'edit resource' => 'Edit image_recognitions',
    'destroy resource' => 'Destroy image_recognitions',
    'title' => [
        'image_recognitions' => 'Image_recognition',
        'create image_recognition' => 'Create a image_recognition',
        'edit image_recognition' => 'Edit a image_recognition',
    ],
    'button' => [
        'create image_recognition' => 'Create a image_recognition',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
