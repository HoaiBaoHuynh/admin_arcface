<?php

return [
    'list resource' => 'List video_recognitions',
    'create resource' => 'Create video_recognitions',
    'edit resource' => 'Edit video_recognitions',
    'destroy resource' => 'Destroy video_recognitions',
    'title' => [
        'video_recognitions' => 'Video_recognition',
        'create video_recognition' => 'Create a video_recognition',
        'edit video_recognition' => 'Edit a video_recognition',
        'export excel' => 'Xuất Excel',
    ],
    'button' => [
        'create video_recognition' => 'Create a video_recognition',
        'export excel' => 'Xuất file Excel',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
