<?php

return [
    'list resource' => 'List real_time_recognitions',
    'create resource' => 'Create real_time_recognitions',
    'edit resource' => 'Edit real_time_recognitions',
    'destroy resource' => 'Destroy real_time_recognitions',
    'title' => [
        'real_time_recognitions' => 'Real_time_recognition',
        'create real_time_recognition' => 'Create a real_time_recognition',
        'edit real_time_recognition' => 'Edit a real_time_recognition',
    ],
    'button' => [
        'create real_time_recognition' => 'Create a real_time_recognition',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
