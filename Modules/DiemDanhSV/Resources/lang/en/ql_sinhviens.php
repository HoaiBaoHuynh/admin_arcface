<?php

return [
    'list resource' => 'List ql_sinhviens',
    'create resource' => 'Create student information',
    'edit resource' => 'Edit ql_sinhviens',
    'destroy resource' => 'Destroy ql_sinhviens',
    'title' => [
        'ql_sinhviens' => 'QL_SinhVien',
        'create ql_sinhvien' => 'Create a student information',
        'edit ql_sinhvien' => 'Edit a ql_sinhvien',
    ],
    'button' => [
        'create ql_sinhvien' => 'Create a student information',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
