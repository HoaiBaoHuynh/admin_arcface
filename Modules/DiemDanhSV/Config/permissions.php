<?php

return [
    'diemdanhsv.real_time_recognitions' => [
        'index' => 'diemdanhsv::real_time_recognitions.list resource',
        'create' => 'diemdanhsv::real_time_recognitions.create resource',
        'edit' => 'diemdanhsv::real_time_recognitions.edit resource',
        'destroy' => 'diemdanhsv::real_time_recognitions.destroy resource',
    ],
    'diemdanhsv.video_recognitions' => [
        'index' => 'diemdanhsv::video_recognitions.list resource',
        'create' => 'diemdanhsv::video_recognitions.create resource',
        'edit' => 'diemdanhsv::video_recognitions.edit resource',
        'destroy' => 'diemdanhsv::video_recognitions.destroy resource',
    ],
    'diemdanhsv.image_recognitions' => [
        'index' => 'diemdanhsv::image_recognitions.list resource',
        'create' => 'diemdanhsv::image_recognitions.create resource',
        'edit' => 'diemdanhsv::image_recognitions.edit resource',
        'destroy' => 'diemdanhsv::image_recognitions.destroy resource',
    ],
    'diemdanhsv.ql_sinhviens' => [
        'index' => 'diemdanhsv::ql_sinhviens.list resource',
        'create' => 'diemdanhsv::ql_sinhviens.create resource',
        'edit' => 'diemdanhsv::ql_sinhviens.edit resource',
        'destroy' => 'diemdanhsv::ql_sinhviens.destroy resource',
    ],
// append




];
