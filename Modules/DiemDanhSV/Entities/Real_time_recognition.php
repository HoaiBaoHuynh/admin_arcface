<?php

namespace Modules\DiemDanhSV\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Real_time_recognition extends Model
{
    use Translatable;

    protected $table = 'diemdanhsv';
    public $translatedAttributes = [];
    protected $fillable = [];
}
