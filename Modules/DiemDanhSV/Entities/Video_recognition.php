<?php

namespace Modules\DiemDanhSV\Entities;

use Illuminate\Database\Eloquent\Model;

class Video_recognition extends Model
{
    protected $table = 'diemdanhsv';
    protected $fillable = ['id', 'MSSV','HoDem','Ten','Lop'];
}
