<?php

namespace Modules\DiemDanhSV\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Image_recognition extends Model
{
    use Translatable;

    protected $table = 'diemdanhsv__image_recognitions';
    public $translatedAttributes = [];
    protected $fillable = [];
}
