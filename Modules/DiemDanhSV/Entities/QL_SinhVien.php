<?php

namespace Modules\DiemDanhSV\Entities;

use Illuminate\Database\Eloquent\Model;

class QL_SinhVien extends Model
{

    protected $table = 'diemdanhsv';
    protected $fillable = ['MSSV','HoDem','Ten','Lop','image'];
    public $timestamps =- false;
}
