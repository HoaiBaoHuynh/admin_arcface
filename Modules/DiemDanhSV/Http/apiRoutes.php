<?php

use Illuminate\Routing\Router;

$router->group(['prefix' => 'diemdanh'], function (Router $router) {
	$router->get('/video', [
		'as' => 'api.diemdanhsv.video_recognition.info',
		'uses' => 'Video_recognitionController@index',
	]);
});