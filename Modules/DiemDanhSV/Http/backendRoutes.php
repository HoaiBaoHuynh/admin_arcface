<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/diemdanhsv'], function (Router $router) {
    $router->bind('real_time_recognition', function ($id) {
        return app('Modules\DiemDanhSV\Repositories\Real_time_recognitionRepository')->find($id);
    });
    $router->get('real_time_recognitions', [
        'as' => 'admin.diemdanhsv.real_time_recognition.index',
        'uses' => 'Real_time_recognitionController@index',
        'middleware' => 'can:diemdanhsv.real_time_recognitions.index'
    ]);
    $router->get('real_time_recognitions_ajax', [
        'as' => 'admin.diemdanhsv.real_time_recognition.ajax',
        'uses' => 'Real_time_recognitionController@readAjax',
    ]);
    $router->get('real_time_recognitions_unkown', [
        'as' => 'admin.diemdanhsv.real_time_recognition.unkown',
        'uses' => 'Real_time_recognitionController@readUnknown',
    ]);
    $router->get('real_time_recognitions/create', [
        'as' => 'admin.diemdanhsv.real_time_recognition.create',
        'uses' => 'Real_time_recognitionController@create',
        'middleware' => 'can:diemdanhsv.real_time_recognitions.create'
    ]);
    $router->post('real_time_recognitions', [
        'as' => 'admin.diemdanhsv.real_time_recognition.store',
        'uses' => 'Real_time_recognitionController@store',
        'middleware' => 'can:diemdanhsv.real_time_recognitions.create'
    ]);
    $router->get('real_time_recognitions/{real_time_recognition}/edit', [
        'as' => 'admin.diemdanhsv.real_time_recognition.edit',
        'uses' => 'Real_time_recognitionController@edit',
        'middleware' => 'can:diemdanhsv.real_time_recognitions.edit'
    ]);
    $router->put('real_time_recognitions/{real_time_recognition}', [
        'as' => 'admin.diemdanhsv.real_time_recognition.update',
        'uses' => 'Real_time_recognitionController@update',
        'middleware' => 'can:diemdanhsv.real_time_recognitions.edit'
    ]);
    $router->delete('real_time_recognitions/{real_time_recognition}', [
        'as' => 'admin.diemdanhsv.real_time_recognition.destroy',
        'uses' => 'Real_time_recognitionController@destroy',
        'middleware' => 'can:diemdanhsv.real_time_recognitions.destroy'
    ]);
    $router->bind('video_recognition', function ($id) {
        return app('Modules\DiemDanhSV\Repositories\Video_recognitionRepository')->find($id);
    });
    $router->get('video_recognitions', [
        'as' => 'admin.diemdanhsv.video_recognition.index',
        'uses' => 'Video_recognitionController@index',
        'middleware' => 'can:diemdanhsv.video_recognitions.index'
    ]);
    $router->get('video_recognitions/create', [
        'as' => 'admin.diemdanhsv.video_recognition.create',
        'uses' => 'Video_recognitionController@create',
        'middleware' => 'can:diemdanhsv.video_recognitions.create'
    ]);
    $router->post('video_recognitions', [
        'as' => 'admin.diemdanhsv.video_recognition.store',
        'uses' => 'Video_recognitionController@store',
        'middleware' => 'can:diemdanhsv.video_recognitions.create'
    ]);
    $router->post('import_excels', [
        'as' => 'admin.diemdanhsv.video_recognition.import_excel',
        'uses' => 'Video_recognitionController@import_excel',
    ]);
    $router->post('export_excels', [
        'as' => 'admin.diemdanhsv.video_recognition.export_excels',
        'uses' => 'Video_recognitionController@export_excel',
    ]);
    $router->post('diemdanh_hinhanh', [
        'as' => 'admin.diemdanhsv.video_recognition.diemdanh_hinhanh',
        'uses' => 'Video_recognitionController@diemdanh_hinhanh',
    ]);
    $router->post('diemdanh_video', [
        'as' => 'admin.diemdanhsv.video_recognition.diemdanh_video',
        'uses' => 'Video_recognitionController@diemdanh_video',
    ]);
    $router->post('camera_start', [
        'as' => 'admin.diemdanhsv.video_recognition.camera_start',
        'uses' => 'Video_recognitionController@camera_start',
    ]);
    $router->get('camera_end', [
        'as' => 'admin.diemdanhsv.video_recognition.camera_end',
        'uses' => 'Video_recognitionController@camera_end',
    ]);

    $router->get('video_recognitions/{video_recognition}/edit', [
        'as' => 'admin.diemdanhsv.video_recognition.edit',
        'uses' => 'Video_recognitionController@edit',
        'middleware' => 'can:diemdanhsv.video_recognitions.edit'
    ]);
    $router->put('video_recognitions/{video_recognition}', [
        'as' => 'admin.diemdanhsv.video_recognition.update',
        'uses' => 'Video_recognitionController@update',
        'middleware' => 'can:diemdanhsv.video_recognitions.edit'
    ]);
    $router->delete('video_recognitions/{video_recognition}', [
        'as' => 'admin.diemdanhsv.video_recognition.destroy',
        'uses' => 'Video_recognitionController@destroy',
        'middleware' => 'can:diemdanhsv.video_recognitions.destroy'
    ]);
    $router->bind('image_recognition', function ($id) {
        return app('Modules\DiemDanhSV\Repositories\Image_recognitionRepository')->find($id);
    });
    $router->get('image_recognitions', [
        'as' => 'admin.diemdanhsv.image_recognition.index',
        'uses' => 'Image_recognitionController@index',
        'middleware' => 'can:diemdanhsv.image_recognitions.index'
    ]);
    $router->get('image_recognitions/create', [
        'as' => 'admin.diemdanhsv.image_recognition.create',
        'uses' => 'Image_recognitionController@create',
        'middleware' => 'can:diemdanhsv.image_recognitions.create'
    ]);
    $router->post('image_recognitions', [
        'as' => 'admin.diemdanhsv.image_recognition.store',
        'uses' => 'Image_recognitionController@store',
        'middleware' => 'can:diemdanhsv.image_recognitions.create'
    ]);
    $router->get('image_recognitions/{image_recognition}/edit', [
        'as' => 'admin.diemdanhsv.image_recognition.edit',
        'uses' => 'Image_recognitionController@edit',
        'middleware' => 'can:diemdanhsv.image_recognitions.edit'
    ]);
    $router->put('image_recognitions/{image_recognition}', [
        'as' => 'admin.diemdanhsv.image_recognition.update',
        'uses' => 'Image_recognitionController@update',
        'middleware' => 'can:diemdanhsv.image_recognitions.edit'
    ]);
    $router->delete('image_recognitions/{image_recognition}', [
        'as' => 'admin.diemdanhsv.image_recognition.destroy',
        'uses' => 'Image_recognitionController@destroy',
        'middleware' => 'can:diemdanhsv.image_recognitions.destroy'
    ]);
    $router->bind('ql_sinhvien', function ($id) {
        return app('Modules\DiemDanhSV\Repositories\QL_SinhVienRepository')->find($id);
    });
    $router->get('ql_sinhviens', [
        'as' => 'admin.diemdanhsv.ql_sinhvien.index',
        'uses' => 'QL_SinhVienController@index',
        'middleware' => 'can:diemdanhsv.ql_sinhviens.index'
    ]);
    $router->get('ql_sinhviens/create', [
        'as' => 'admin.diemdanhsv.ql_sinhvien.create',
        'uses' => 'QL_SinhVienController@create',
        'middleware' => 'can:diemdanhsv.ql_sinhviens.create'
    ]);
    $router->post('ql_sinhviens', [
        'as' => 'admin.diemdanhsv.ql_sinhvien.store',
        'uses' => 'QL_SinhVienController@store',
        'middleware' => 'can:diemdanhsv.ql_sinhviens.create'
    ]);
    $router->get('ql_sinhviens/{ql_sinhvien}/edit', [
        'as' => 'admin.diemdanhsv.ql_sinhvien.edit',
        'uses' => 'QL_SinhVienController@edit',
        'middleware' => 'can:diemdanhsv.ql_sinhviens.edit'
    ]);
    $router->put('ql_sinhviens/{ql_sinhvien}', [
        'as' => 'admin.diemdanhsv.ql_sinhvien.update',
        'uses' => 'QL_SinhVienController@update',
        'middleware' => 'can:diemdanhsv.ql_sinhviens.edit'
    ]);
    $router->delete('ql_sinhviens/{ql_sinhvien}', [
        'as' => 'admin.diemdanhsv.ql_sinhvien.destroy',
        'uses' => 'QL_SinhVienController@destroy',
        'middleware' => 'can:diemdanhsv.ql_sinhviens.destroy'
    ]);
// append




});
