<?php

namespace Modules\DiemDanhSV\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller;
use Modules\DiemDanhSV\Entities\Video_recognition;
use Illuminate\Support\Facades\DB;
use Modules\DiemDanhSV\Transformers\DiemDanhSVTransformer;
use Excel;

class Video_recognitionController extends Controller
{
	public function index(Request $request){
		/*$path = $request->file('select_file')->getRealPath();
        $data = Excel::load($path)->get();
		$sv = $data->toArray();*/
		$sv = Video_recognition::select('Ten')->get();
		return $sv;
	}
}