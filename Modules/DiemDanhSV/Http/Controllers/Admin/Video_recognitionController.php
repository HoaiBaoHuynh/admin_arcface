<?php

namespace Modules\DiemDanhSV\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\DiemDanhSV\Entities\Video_recognition;
use Modules\DiemDanhSV\Entities\QL_SinhVien;
use Modules\DiemDanhSV\Http\Requests\CreateVideo_recognitionRequest;
use Modules\DiemDanhSV\Http\Requests\UpdateVideo_recognitionRequest;
use Modules\DiemDanhSV\Repositories\Video_recognitionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\DB;
use Excel;
use Validator;

class Video_recognitionController extends AdminBaseController
{
    /**
     * @var Video_recognitionRepository
     */
    private $video_recognition;

    public function __construct(Video_recognitionRepository $video_recognition)
    {
        parent::__construct();

        $this->video_recognition = $video_recognition;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $video_recognitions = DB::table('testing')->get();
        $dsTuan = DB::table('DS_Tuan')->get();
        $dsMaLop = DB::table('ds_malop')->get();

        return view('diemdanhsv::admin.video_recognitions.index', compact('video_recognitions','dsTuan','dsMaLop'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('diemdanhsv::admin.video_recognitions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateVideo_recognitionRequest $request
     * @return Response
     */
    public function store(CreateVideo_recognitionRequest $request)
    {
        $this->video_recognition->create($request->all());

        return redirect()->route('admin.diemdanhsv.video_recognition.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('diemdanhsv::video_recognitions.title.video_recognitions')]));
    }

    public function import_excel(Request $request){
        $validator = \Validator::make($request->all(), [
            'select_file[]' => 'mimes:xls,xlsx',]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator);
        }
        if (empty($request->select_file)) {
            return redirect()->route('admin.diemdanhsv.video_recognition.index')->withErrors('Please choose file excel to upload.');
        }
        $dsmalop = DB::table('ds_malop')->get();
        $files = $request->file('select_file');
        foreach ($files as $file) {
            $path = $file->getRealPath();
            $data = Excel::load($path)->get();
            $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $bien = DB::table('ds_malop')->where('ma_lop','=',$filename)->select('ma_lop')->get();
            if ($bien == "[]") {
                if($data->count() > 0 )
                {
                    foreach ($data->toArray() as $key => $value) {
                        foreach ($value as $row) {
                            $insert_data[] = array(
                                'MSSV' => $row['ma_sv'],
                                'Ma_Lop' => $filename,
                                'HoDem' => $row['ho_dem'],
                                'Ten' => $row['ten'],
                                'Lop' => $row['lop_hoc'],
                                'Tuan_1' => $row['tuan_1'],
                                'Tuan_2' => $row['tuan_2'],
                                'Tuan_3' => $row['tuan_3'],
                                'Tuan_4' => $row['tuan_4'],
                                'Tuan_5' => $row['tuan_5'],
                                'Tuan_6' => $row['tuan_6'],
                                'Tuan_7' => $row['tuan_7'],
                                'Tuan_8' => $row['tuan_8'],
                                'Tuan_9' => $row['tuan_9'],
                                'Tuan_10' => $row['tuan_10'],
                                'Tuan_11' => $row['tuan_11'],
                                'Tuan_12' => $row['tuan_12'],
                                'Tuan_13' => $row['tuan_13'],
                                'Tuan_14' => $row['tuan_14'],
                                'Tuan_15' => $row['tuan_15'],
                            );
                        }
                    }
                    DB::table('ds_malop')->insert(['ma_lop' => $filename,]);
                    if (!empty($insert_data)) {
                        DB::table('testing')->insert($insert_data);
                        unset($insert_data);
                    }
                }
            }
        }
        return redirect()->route('admin.diemdanhsv.video_recognition.index')->withSuccess('Excel Data Imported successfully.');
    }

    public function export_excel(Request $request){
        $list_export = DB::table('testing')->where('Ma_Lop','=',$request->malop_1)->get();
        $student_array[] = array('STT','Mã SV','Mã Lớp','Họ Đệm','Tên','Lớp Học','Tuần 1','Tuần 2','Tuần 3','Tuần 4','Tuần 5','Tuần 6','Tuần 7','Tuần 8','Tuần 9','Tuần 10','Tuần 11','Tuần 12','Tuần 13','Tuần 14','Tuần 15');
        $i = 1;
        foreach ($list_export as $rows) {
            $student_array[] = array(
                'STT' => $i,
                'Mã SV' => $rows->MSSV,
                'Mã Lớp' => $rows->Ma_Lop,
                'Họ Đệm' => $rows->HoDem,
                'Tên' => $rows->Ten,
                'Lớp Học' => $rows->Lop,
                'Tuần 1' => $rows->Tuan_1,
                'Tuần 2' => $rows->Tuan_2,
                'Tuần 3' => $rows->Tuan_3,
                'Tuần 4' => $rows->Tuan_4,
                'Tuần 5' => $rows->Tuan_5,
                'Tuần 6' => $rows->Tuan_6,
                'Tuần 7' => $rows->Tuan_7,
                'Tuần 8' => $rows->Tuan_8,
                'Tuần 9' => $rows->Tuan_9,
                'Tuần 10' => $rows->Tuan_10,
                'Tuần 11' => $rows->Tuan_11,
                'Tuần 12' => $rows->Tuan_12,
                'Tuần 13' => $rows->Tuan_13,
                'Tuần 14' => $rows->Tuan_14,
                'Tuần 15' => $rows->Tuan_15,
            );
            $i+=1;
        }
        Excel::create($request->malop_1, function($excel) use($student_array){
            $excel->setTitle('Student data');
            $excel->sheet('Student data', function($sheet) use($student_array){
                $sheet->fromArray($student_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }

    public function diemdanh_hinhanh(Request $request){
        $validator = \Validator::make($request->all(), [
                'image_file[]' => 'mimes:jpeg,jpg,png|max:10000',
            ]);
        if ($validator->fails()) {
            return redirect()->route('admin.diemdanhsv.video_recognition.index')->withErrors($validator);
        }
        if (empty($request->image_file)) {
            return redirect()->route('admin.diemdanhsv.video_recognition.index')->withErrors('Please choose image to upload.');
        }
        if ($request->hasFile('image_file')) {
            $list_mssv = array();
            $img_files = $request->file('image_file');
            foreach ($img_files as $img) {
                $img_name = $img->getClientOriginalName();
                $img->move('assets/images_2',$img_name);

                $url = "http://127.0.0.1:5000/image/".$img_name;
                // create & initialize a curl session
                $curl = curl_init();
                // set our url with curl_setopt()
                curl_setopt($curl, CURLOPT_URL, $url);
                // return the transfer as a string, also with setopt()
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                // curl_exec() executes the started curl session
                // $output contains the output string
                $output = curl_exec($curl);
                // close curl resource to free up system resources
                // (deletes the variable made by curl_init)
                curl_close($curl);
                $result_1 = json_decode($output);
                if (empty($result_1)) {
                    return redirect()->route('admin.diemdanhsv.video_recognition.index')->withErrors('API is not available!');
                }
                foreach ($result_1 as $value) {
                    array_push($list_mssv, $value);
                }
            }
            $list_mssv = array_unique($list_mssv);
            foreach ($list_mssv as $mssv) {
                DB::table('testing')->where([
                    ['MSSV','=',$mssv],
                    /*['Ma_Lop', '=', $request->malop_old],*/
                ])->update([$request->tuan => 1]);
            }
        }
        return redirect()->route('admin.diemdanhsv.video_recognition.index')->withSuccess('Sucessfully',['name' => trans('diemdanhsv::video_recognitions.title.video_recognitions')]);

    }
    public function diemdanh_video(Request $request){
        $validator = \Validator::make($request->all(), [
                'video_file' => 'mimes:mp4,avi,mov,webm|max: 100000',
            ]);
        if ($validator->fails()) {
            return redirect()->route('admin.diemdanhsv.video_recognition.index')->withErrors($validator);
        }
        if (empty($request->video_file)) {
            return redirect()->route('admin.diemdanhsv.video_recognition.index')->withErrors('Please choose video to upload.');
        }
        if ($request->hasFile('video_file')) {
            $file = $request->file('video_file');
            $name_file = $file->getClientOriginalName();
            $file->move('assets/videos',$name_file);

            $url = "http://127.0.0.1:5000/student/".$name_file;
            // create & initialize a curl session
            $curl = curl_init();
            // set our url with curl_setopt()
            curl_setopt($curl, CURLOPT_URL, $url);
            // return the transfer as a string, also with setopt()
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            // curl_exec() executes the started curl session
            // $output contains the output string
            $output = curl_exec($curl);
            // close curl resource to free up system resources
            // (deletes the variable made by curl_init)
            curl_close($curl);
            $result = json_decode($output);
            if (empty($result)) {
                return redirect()->route('admin.diemdanhsv.video_recognition.index')->withErrors('API is not available!');
            }
            foreach ($result as $key => $value) {
                DB::table('testing')->where([
                    ['MSSV','=',$value],
                    /*['Ma_Lop', '=', $request->malop_old],*/
                ])->update([$request->tuan => 1]);
            }
        }

        return redirect()->route('admin.diemdanhsv.video_recognition.index')->withSuccess('Updated Sucessfully',['name' => trans('diemdanhsv::video_recognitions.title.video_recognitions')]);
    }

    public function camera_start(Request $request){
        $url = "http://127.0.0.1:5000/camera/start";
        // create & initialize a curl session
        $curl = curl_init();
        // set our url with curl_setopt()
        curl_setopt($curl, CURLOPT_URL, $url);
        // return the transfer as a string, also with setopt()
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // curl_exec() executes the started curl session
        // $output contains the output string
        $output = curl_exec($curl);
        // close curl resource to free up system resources
        // (deletes the variable made by curl_init)
        curl_close($curl);
        $result = json_decode($output);
        if (empty($result)) {
            return redirect()->route('admin.diemdanhsv.video_recognition.index')->withErrors('API is not available!');
        }
        foreach ($result as $key => $value) {
            DB::table('testing')->where([
                ['MSSV','=',$value],
                /*['Ma_Lop', '=', $request->malop_old],*/
            ])->update([$request->tuan => 1]);
        }
        return redirect()->route('admin.diemdanhsv.video_recognition.index')->withSuccess('Updated Sucessfully',['name' => trans('diemdanhsv::video_recognitions.title.video_recognitions')]);
    }

    public function camera_end(){
        $url = "http://127.0.0.1:5000/camera/end";
        // create & initialize a curl session
        $curl = curl_init();
        // set our url with curl_setopt()
        curl_setopt($curl, CURLOPT_URL, $url);
        // return the transfer as a string, also with setopt()
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // curl_exec() executes the started curl session
        // $output contains the output string
        $output = curl_exec($curl);
        // close curl resource to free up system resources
        // (deletes the variable made by curl_init)
        curl_close($curl);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Video_recognition $video_recognition
     * @return Response
     */
    public function edit(Video_recognition $video_recognition)
    {
        return view('diemdanhsv::admin.video_recognitions.edit', compact('video_recognition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Video_recognition $video_recognition
     * @param  UpdateVideo_recognitionRequest $request
     * @return Response
     */
    public function update(Video_recognition $video_recognition, UpdateVideo_recognitionRequest $request)
    {
        $this->video_recognition->update($video_recognition, $request->all());

        return redirect()->route('admin.diemdanhsv.video_recognition.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('diemdanhsv::video_recognitions.title.video_recognitions')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Video_recognition $video_recognition
     * @return Response
     */
    public function destroy(Video_recognition $video_recognition)
    {
        $this->video_recognition->destroy($video_recognition);

        return redirect()->route('admin.diemdanhsv.video_recognition.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('diemdanhsv::video_recognitions.title.video_recognitions')]));
    }
}
