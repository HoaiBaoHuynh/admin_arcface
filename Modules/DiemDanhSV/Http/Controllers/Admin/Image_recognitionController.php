<?php

namespace Modules\DiemDanhSV\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\DiemDanhSV\Entities\Image_recognition;
use Modules\DiemDanhSV\Http\Requests\CreateImage_recognitionRequest;
use Modules\DiemDanhSV\Http\Requests\UpdateImage_recognitionRequest;
use Modules\DiemDanhSV\Repositories\Image_recognitionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class Image_recognitionController extends AdminBaseController
{
    /**
     * @var Image_recognitionRepository
     */
    private $image_recognition;

    public function __construct(Image_recognitionRepository $image_recognition)
    {
        parent::__construct();

        $this->image_recognition = $image_recognition;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$image_recognitions = $this->image_recognition->all();

        return view('diemdanhsv::admin.image_recognitions.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('diemdanhsv::admin.image_recognitions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateImage_recognitionRequest $request
     * @return Response
     */
    public function store(CreateImage_recognitionRequest $request)
    {
        $this->image_recognition->create($request->all());

        return redirect()->route('admin.diemdanhsv.image_recognition.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('diemdanhsv::image_recognitions.title.image_recognitions')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Image_recognition $image_recognition
     * @return Response
     */
    public function edit(Image_recognition $image_recognition)
    {
        return view('diemdanhsv::admin.image_recognitions.edit', compact('image_recognition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Image_recognition $image_recognition
     * @param  UpdateImage_recognitionRequest $request
     * @return Response
     */
    public function update(Image_recognition $image_recognition, UpdateImage_recognitionRequest $request)
    {
        $this->image_recognition->update($image_recognition, $request->all());

        return redirect()->route('admin.diemdanhsv.image_recognition.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('diemdanhsv::image_recognitions.title.image_recognitions')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Image_recognition $image_recognition
     * @return Response
     */
    public function destroy(Image_recognition $image_recognition)
    {
        $this->image_recognition->destroy($image_recognition);

        return redirect()->route('admin.diemdanhsv.image_recognition.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('diemdanhsv::image_recognitions.title.image_recognitions')]));
    }
}
