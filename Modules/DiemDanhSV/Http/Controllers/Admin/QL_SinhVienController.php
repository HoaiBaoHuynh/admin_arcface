<?php

namespace Modules\DiemDanhSV\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\DiemDanhSV\Entities\QL_SinhVien;
use Modules\DiemDanhSV\Http\Requests\CreateQL_SinhVienRequest;
use Modules\DiemDanhSV\Http\Requests\UpdateQL_SinhVienRequest;
use Modules\DiemDanhSV\Repositories\QL_SinhVienRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\DB;

class QL_SinhVienController extends AdminBaseController
{
    /**
     * @var QL_SinhVienRepository
     */
    private $ql_sinhvien;

    public function __construct(QL_SinhVienRepository $ql_sinhvien)
    {
        parent::__construct();

        $this->ql_sinhvien = $ql_sinhvien;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $ql_sinhviens = QL_SinhVien::all();

        return view('diemdanhsv::admin.ql_sinhviens.index', compact('ql_sinhviens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('diemdanhsv::admin.ql_sinhviens.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateQL_SinhVienRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        $listsv = new QL_SinhVien();
        $listsv->MSSV = $request->input('mssv');
        $listsv->HoDem = $request->input('hodem');
        $listsv->Ten = $request->input('ten');
        $listsv->Lop = $request->input('lop');
        if($request->hasFile('hinh')){
            $file = $request->file('hinh');
            $extension = $file->getClientOriginalName();
            $file->move('assets/images',$extension);
            $listsv->image = $extension;
        }
        $listsv->save();

        return redirect()->route('admin.diemdanhsv.ql_sinhvien.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('diemdanhsv::ql_sinhviens.title.ql_sinhviens')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  QL_SinhVien $ql_sinhvien
     * @return Response
     */
    public function edit(QL_SinhVien $ql_sinhvien)
    {
        return view('diemdanhsv::admin.ql_sinhviens.edit', compact('ql_sinhvien'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  QL_SinhVien $ql_sinhvien
     * @param  UpdateQL_SinhVienRequest $request
     * @return Response
     */
    public function update(QL_SinhVien $ql_sinhvien, UpdateQL_SinhVienRequest $request)
    {
        $editdsv = QL_SinhVien::find($ql_sinhvien->id);
        $editdsv->MSSV = $request->input('mssv');
        $editdsv->HoDem = $request->input('hodem');
        $editdsv->Ten = $request->input('ten');
        $editdsv->Lop = $request->input('lop');
        if($request->hasFile('hinh')){
            $file = $request->file('hinh');
            $extension = $file->getClientOriginalName();
            $file->move('assets/images/',$extension);
            $editdsv->image = $extension;
        }

        $editdsv->save();
        return redirect()->route('admin.diemdanhsv.ql_sinhvien.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('diemdanhsv::ql_sinhviens.title.ql_sinhviens')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  QL_SinhVien $ql_sinhvien
     * @return Response
     */
    public function destroy(QL_SinhVien $ql_sinhvien)
    {
        $this->ql_sinhvien->destroy($ql_sinhvien);

        return redirect()->route('admin.diemdanhsv.ql_sinhvien.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('diemdanhsv::ql_sinhviens.title.ql_sinhviens')]));
    }
}
