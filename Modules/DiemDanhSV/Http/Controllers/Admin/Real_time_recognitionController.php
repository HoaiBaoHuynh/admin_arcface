<?php

namespace Modules\DiemDanhSV\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\DiemDanhSV\Entities\Real_time_recognition;
use Modules\DiemDanhSV\Http\Requests\CreateReal_time_recognitionRequest;
use Modules\DiemDanhSV\Http\Requests\UpdateReal_time_recognitionRequest;
use Modules\DiemDanhSV\Repositories\Real_time_recognitionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class Real_time_recognitionController extends AdminBaseController
{
    /**
     * @var Real_time_recognitionRepository
     */
    private $real_time_recognition;

    public function __construct(Real_time_recognitionRepository $real_time_recognition)
    {
        parent::__construct();

        $this->real_time_recognition = $real_time_recognition;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        /*$unknowns = DB::table('check_logs')->where('MSSV','like','%unknown%')->get();*/
        /*$values = DB::table('diemdanhsv')->join('check_logs','diemdanhsv.MSSV','=','check_logs.MSSV')->select('check_logs.id','diemdanhsv.image', 'check_logs.image as img_compare','check_logs.MSSV as MSSV','HoDem','Ten','Lop','time')->get();*/
        /*$list_unknowns = array();
        $list_duplicate = array();
        foreach ($unknowns as $rt) {
            if (!in_array($rt->MSSV, $list_duplicate)) {
                array_push($list_duplicate, $rt->MSSV);
                array_push($list_unknowns,(object)[
                    'image' => $rt->image,
                    'MSSV' => $rt->MSSV,
                    'time' => $rt->time,
                ]);
            }
        }
        $obj = (object)$list_unknowns;
        $values2 = Collection::make($obj);*/
        return view('diemdanhsv::admin.real_time_recognitions.index', compact(''));
    }

    public function readAjax(){
        $real_time_recognitions = DB::table('diemdanhsv')->join('check_logs','diemdanhsv.MSSV','=','check_logs.MSSV')->select('check_logs.id','diemdanhsv.image', 'check_logs.image as img_compare','check_logs.MSSV as MSSV','HoDem','Ten','Lop','time')->get();
        $list_recognition = array();
        $list_mssv = array();
        foreach ($real_time_recognitions as $rt) {
            if (!in_array($rt->MSSV, $list_mssv)) {
                array_push($list_mssv, $rt->MSSV);
                array_push($list_recognition,(object)[
                    'id' => $rt->id,
                    'image' => $rt->image,
                    'img_compare' => $rt->img_compare,
                    'MSSV' => $rt->MSSV,
                    'HoDem' => $rt->HoDem,
                    'Ten' => $rt->Ten,
                    'Lop' => $rt->Lop,
                    'time' => $rt->time,
                ]);
            }
        }
        $obj = (object)$list_recognition;
        $values = Collection::make($obj);
        return view('diemdanhsv::admin.real_time_recognitions.studentList', compact('values'));
    }
    public function readUnknown(){
        $unknowns = DB::table('check_logs')->where('MSSV','like','%unknown%')->get();
        $list_unknowns = array();
        $list_duplicate = array();
        foreach ($unknowns as $rt) {
            if (!in_array($rt->MSSV, $list_duplicate)) {
                array_push($list_duplicate, $rt->MSSV);
                array_push($list_unknowns,(object)[
                    'image' => $rt->image,
                    'MSSV' => $rt->MSSV,
                    'time' => $rt->time,
                ]);
            }
        }
        $obj = (object)$list_unknowns;
        $values2 = Collection::make($obj);

        return view('diemdanhsv::admin.real_time_recognitions.unknownList', compact('values2'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('diemdanhsv::admin.real_time_recognitions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateReal_time_recognitionRequest $request
     * @return Response
     */
    public function store(CreateReal_time_recognitionRequest $request)
    {
        $this->real_time_recognition->create($request->all());

        return redirect()->route('admin.diemdanhsv.real_time_recognition.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('diemdanhsv::real_time_recognitions.title.real_time_recognitions')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Real_time_recognition $real_time_recognition
     * @return Response
     */
    public function edit(Real_time_recognition $real_time_recognition)
    {
        return view('diemdanhsv::admin.real_time_recognitions.edit', compact('real_time_recognition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Real_time_recognition $real_time_recognition
     * @param  UpdateReal_time_recognitionRequest $request
     * @return Response
     */
    public function update(Real_time_recognition $real_time_recognition, UpdateReal_time_recognitionRequest $request)
    {
        $this->real_time_recognition->update($real_time_recognition, $request->all());

        return redirect()->route('admin.diemdanhsv.real_time_recognition.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('diemdanhsv::real_time_recognitions.title.real_time_recognitions')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Real_time_recognition $real_time_recognition
     * @return Response
     */
    public function destroy(Real_time_recognition $real_time_recognition)
    {
        $this->real_time_recognition->destroy($real_time_recognition);

        return redirect()->route('admin.diemdanhsv.real_time_recognition.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('diemdanhsv::real_time_recognitions.title.real_time_recognitions')]));
    }
}
