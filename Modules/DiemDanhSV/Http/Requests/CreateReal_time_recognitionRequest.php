<?php

namespace Modules\DiemDanhSV\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateReal_time_recognitionRequest extends BaseFormRequest
{
    public function rules()
    {
        return [];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
