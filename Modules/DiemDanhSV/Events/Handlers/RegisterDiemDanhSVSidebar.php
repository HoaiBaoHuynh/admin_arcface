<?php

namespace Modules\DiemDanhSV\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterDiemDanhSVSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('DiemDanhSV'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('diemdanhsv::real_time_recognitions.title.real_time_recognitions'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.diemdanhsv.real_time_recognition.create');
                    $item->route('admin.diemdanhsv.real_time_recognition.index');
                    $item->authorize(
                        $this->auth->hasAccess('diemdanhsv.real_time_recognitions.index')
                    );
                });
                $item->item(trans('diemdanhsv::video_recognitions.title.video_recognitions'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.diemdanhsv.video_recognition.create');
                    $item->route('admin.diemdanhsv.video_recognition.index');
                    $item->authorize(
                        $this->auth->hasAccess('diemdanhsv.video_recognitions.index')
                    );
                });
                $item->item(trans('diemdanhsv::image_recognitions.title.image_recognitions'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.diemdanhsv.image_recognition.create');
                    $item->route('admin.diemdanhsv.image_recognition.index');
                    $item->authorize(
                        $this->auth->hasAccess('diemdanhsv.image_recognitions.index')
                    );
                });
                $item->item(trans('diemdanhsv::ql_sinhviens.title.ql_sinhviens'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.diemdanhsv.ql_sinhvien.create');
                    $item->route('admin.diemdanhsv.ql_sinhvien.index');
                    $item->authorize(
                        $this->auth->hasAccess('diemdanhsv.ql_sinhviens.index')
                    );
                });
// append




            });
        });

        return $menu;
    }
}
