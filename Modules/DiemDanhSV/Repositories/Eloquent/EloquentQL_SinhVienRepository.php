<?php

namespace Modules\DiemDanhSV\Repositories\Eloquent;

use Modules\DiemDanhSV\Repositories\QL_SinhVienRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentQL_SinhVienRepository extends EloquentBaseRepository implements QL_SinhVienRepository
{
}
