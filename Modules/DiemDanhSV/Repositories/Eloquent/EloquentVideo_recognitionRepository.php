<?php

namespace Modules\DiemDanhSV\Repositories\Eloquent;

use Modules\DiemDanhSV\Repositories\Video_recognitionRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentVideo_recognitionRepository extends EloquentBaseRepository implements Video_recognitionRepository
{
}
