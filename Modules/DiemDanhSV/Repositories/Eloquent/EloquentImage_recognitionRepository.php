<?php

namespace Modules\DiemDanhSV\Repositories\Eloquent;

use Modules\DiemDanhSV\Repositories\Image_recognitionRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentImage_recognitionRepository extends EloquentBaseRepository implements Image_recognitionRepository
{
}
