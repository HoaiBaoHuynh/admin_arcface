<?php

namespace Modules\DiemDanhSV\Repositories\Eloquent;

use Modules\DiemDanhSV\Repositories\Real_time_recognitionRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentReal_time_recognitionRepository extends EloquentBaseRepository implements Real_time_recognitionRepository
{
}
