<?php

namespace Modules\DiemDanhSV\Repositories\Cache;

use Modules\DiemDanhSV\Repositories\QL_SinhVienRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheQL_SinhVienDecorator extends BaseCacheDecorator implements QL_SinhVienRepository
{
    public function __construct(QL_SinhVienRepository $ql_sinhvien)
    {
        parent::__construct();
        $this->entityName = 'diemdanhsv.ql_sinhviens';
        $this->repository = $ql_sinhvien;
    }
}
