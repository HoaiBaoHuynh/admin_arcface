<?php

namespace Modules\DiemDanhSV\Repositories\Cache;

use Modules\DiemDanhSV\Repositories\Video_recognitionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheVideo_recognitionDecorator extends BaseCacheDecorator implements Video_recognitionRepository
{
    public function __construct(Video_recognitionRepository $video_recognition)
    {
        parent::__construct();
        $this->entityName = 'diemdanhsv.video_recognitions';
        $this->repository = $video_recognition;
    }
}
