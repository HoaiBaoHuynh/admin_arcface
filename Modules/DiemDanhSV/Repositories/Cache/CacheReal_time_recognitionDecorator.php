<?php

namespace Modules\DiemDanhSV\Repositories\Cache;

use Modules\DiemDanhSV\Repositories\Real_time_recognitionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheReal_time_recognitionDecorator extends BaseCacheDecorator implements Real_time_recognitionRepository
{
    public function __construct(Real_time_recognitionRepository $real_time_recognition)
    {
        parent::__construct();
        $this->entityName = 'diemdanhsv.real_time_recognitions';
        $this->repository = $real_time_recognition;
    }
}
