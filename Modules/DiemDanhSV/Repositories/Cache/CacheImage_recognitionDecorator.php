<?php

namespace Modules\DiemDanhSV\Repositories\Cache;

use Modules\DiemDanhSV\Repositories\Image_recognitionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheImage_recognitionDecorator extends BaseCacheDecorator implements Image_recognitionRepository
{
    public function __construct(Image_recognitionRepository $image_recognition)
    {
        parent::__construct();
        $this->entityName = 'diemdanhsv.image_recognitions';
        $this->repository = $image_recognition;
    }
}
