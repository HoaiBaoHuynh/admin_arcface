<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiemDanhSVImage_recognitionTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diemdanhsv__image_recognition_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('image_recognition_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['image_recognition_id', 'locale']);
            $table->foreign('image_recognition_id')->references('id')->on('diemdanhsv__image_recognitions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diemdanhsv__image_recognition_translations', function (Blueprint $table) {
            $table->dropForeign(['image_recognition_id']);
        });
        Schema::dropIfExists('diemdanhsv__image_recognition_translations');
    }
}
