<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiemDanhSVVideo_recognitionTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diemdanhsv__video_recognition_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('video_recognition_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['video_recognition_id', 'locale']);
            $table->foreign('video_recognition_id')->references('id')->on('diemdanhsv__video_recognitions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diemdanhsv__video_recognition_translations', function (Blueprint $table) {
            $table->dropForeign(['video_recognition_id']);
        });
        Schema::dropIfExists('diemdanhsv__video_recognition_translations');
    }
}
