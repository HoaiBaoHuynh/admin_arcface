<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiemDanhSVQL_SinhVienTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diemdanhsv__ql_sinhvien_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('ql_sinhvien_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['ql_sinhvien_id', 'locale']);
            $table->foreign('ql_sinhvien_id')->references('id')->on('diemdanhsv__ql_sinhviens')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diemdanhsv__ql_sinhvien_translations', function (Blueprint $table) {
            $table->dropForeign(['ql_sinhvien_id']);
        });
        Schema::dropIfExists('diemdanhsv__ql_sinhvien_translations');
    }
}
