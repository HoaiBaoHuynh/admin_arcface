<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiemDanhSVReal_time_recognitionTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diemdanhsv__real_time_recognition_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('real_time_recognition_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['real_time_recognition_id', 'locale']);
            $table->foreign('real_time_recognition_id')->references('id')->on('diemdanhsv__real_time_recognitions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diemdanhsv__real_time_recognition_translations', function (Blueprint $table) {
            $table->dropForeign(['real_time_recognition_id']);
        });
        Schema::dropIfExists('diemdanhsv__real_time_recognition_translations');
    }
}
