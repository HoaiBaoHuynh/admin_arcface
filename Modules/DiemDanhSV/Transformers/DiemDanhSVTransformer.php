<?php

namespace Modules\DiemDanhSV\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class DiemDanhSVTransformer extends Resource
{
	public function toArray($request){
		return [
			'id' => $this->id,
			'MSSV' => $this->MSSV,
			'HoDem' => $this->HoDem,
			'Ten' => $this->Ten,
			'Lop' => $this->Lop,
			'08/05' => $this['08/05'],
		];
	}
}