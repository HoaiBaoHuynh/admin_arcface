<?php

namespace Modules\DiemDanhSV\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\DiemDanhSV\Events\Handlers\RegisterDiemDanhSVSidebar;

class DiemDanhSVServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterDiemDanhSVSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('real_time_recognitions', array_dot(trans('diemdanhsv::real_time_recognitions')));
            $event->load('video_recognitions', array_dot(trans('diemdanhsv::video_recognitions')));
            $event->load('image_recognitions', array_dot(trans('diemdanhsv::image_recognitions')));
            $event->load('ql_sinhviens', array_dot(trans('diemdanhsv::ql_sinhviens')));
            // append translations




        });
    }

    public function boot()
    {
        $this->publishConfig('diemdanhsv', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\DiemDanhSV\Repositories\Real_time_recognitionRepository',
            function () {
                $repository = new \Modules\DiemDanhSV\Repositories\Eloquent\EloquentReal_time_recognitionRepository(new \Modules\DiemDanhSV\Entities\Real_time_recognition());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\DiemDanhSV\Repositories\Cache\CacheReal_time_recognitionDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\DiemDanhSV\Repositories\Video_recognitionRepository',
            function () {
                $repository = new \Modules\DiemDanhSV\Repositories\Eloquent\EloquentVideo_recognitionRepository(new \Modules\DiemDanhSV\Entities\Video_recognition());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\DiemDanhSV\Repositories\Cache\CacheVideo_recognitionDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\DiemDanhSV\Repositories\Image_recognitionRepository',
            function () {
                $repository = new \Modules\DiemDanhSV\Repositories\Eloquent\EloquentImage_recognitionRepository(new \Modules\DiemDanhSV\Entities\Image_recognition());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\DiemDanhSV\Repositories\Cache\CacheImage_recognitionDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\DiemDanhSV\Repositories\QL_SinhVienRepository',
            function () {
                $repository = new \Modules\DiemDanhSV\Repositories\Eloquent\EloquentQL_SinhVienRepository(new \Modules\DiemDanhSV\Entities\QL_SinhVien());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\DiemDanhSV\Repositories\Cache\CacheQL_SinhVienDecorator($repository);
            }
        );
// add bindings




    }
}
